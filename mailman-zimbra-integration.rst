*******************************************************************
Integrating the Mailman 3 suite with the Zimbra collaboration suite
*******************************************************************

This is a case history of my integration of the Mailman 3 Suite with
Zimbra Collaboration Suite 8.8 (Community Edition).

Background and motivation
#########################

Much of the communication between Mailman and the mta happens with
shared files, so it seems the most straightforward to embed Mailman
alongside Zimbra on the same server. For a small enterprise, a single-server
email solution is convenient.

I had previously integrated Mailman 2.1 with Zimbra 6. That system is
still in operation (for not much longer) serving family email boxes and
hobby mailing lists. The server became out of date with unsupported TLS protocols.
It had become otherwise unmaintainable for spam and virus filtering as well.

In the older system, I had successfully moved Zimbra's instance of httpd
off of port 80, using a new Apache installation to proxy the Zimbra web
UI while also serving the mailman domains.

System platform
###############

* Linode 8GB host
* Ubuntu 18
* Zimbra 8.8
* Mailman Suite 3.3.2

Integration plans
#################

* Leverage the Zimbra proxy server (nginx) to serve the Postorius/Hyperkitty
  web UI.
* Use Zimbra's configuration tools to support the Mailman core mta hooks
* Support letsencrypt certificates for all domains
* virtualenv installation of mailman, django, and uwsgi
* mysql DB for mailman and postorius/hyperkitty

I used the stock installation of Zimbra 8.8 community edition, at /opt/zimbra.
While Zimbra is most at-home when it's the single purpose of a server instance,
it still does a good job of keeping most of its dedicated tools away from
common port numbers and directory trees. That enables the installation of
stock tools like a second MySQL instance.

I built the Mailman virtual env at /opt/mailman/venv.

The domains involved
====================

:mail.whiteoaks.com: The domain mail server, holder of the MX record, main domain
  for the Zimbra mta
:mailman.whiteoaks.com: Default domain for the mailman3 suite installation.
:mail.pasadenabridgeclub.com: Domain for mailing list web site (example)

Zimbra nginx hooks
##################

I found the best place to insert an include hook in the Zimbra proxy
configuration:

In file /opt/zimbra/conf/nginx/templates/nginx.conf.web.template:
::

  # Add includes for other domains (Mailman managed)
  include /opt/mailman/var/etc/nginx/conf.d/*.conf;

This was added at the very bottom of the template, inside the http {} block.

I built the following file tree for nginx include files (still away from root /etc
where another nginx instance might have configuration files):
::

  /opt/mailman/var/etc/nginx:
    dhparam.pem
    mime.types.default
    options-ssl-nginx.conf
    conf.d/
      _mime.types.conf
      mail.whiteoaks.com.conf
      mail.bridgemojo.com.conf
      mailman.whiteoaks.com.conf
      server-example

Interesting nginx problems to work around
=========================================

* Zimbra nginx is built without the uwsgi plugin, so I have to use the http hook
* A few common nginx config files had to be lifted from a standard nginx installation
  for inclusion in the mailman proxies:

  * mime.types.default
  * options-ssl-nginx.conf
  * dhparam.pem

* The base Zimbra proxy does not redirect from http to https

Example files for the mailman proxy
===================================

**_mime.types.conf** to bring in the nginx mime types. (Without this, css and js are served as text/plain.)
::

  include /opt/mailman/var/etc/nginx/mime.types.default;

**mail.whiteoaks.com.conf** added to support redirection of the Zimbra web ui:
::

  # special redirect for Zimbra (not done with default Zimbra proxy)

  server {
      if ($host = mail.whiteoaks.com) {
          return 301 https://$host$request_uri;
      }

      server_name mail.whiteoaks.com;

      listen 80;
      return 404;
  }

**server-example** is a template for all mailman3 domain services. 
Replace ``SERVER_NAME`` with the domain name being served (such as 
mailman.whiteoaks.com):
::

  server {
    server_name SERVER_NAME;
    root /srv/SERVER_NAME;

    listen 443 ssl;
    ssl_certificate /etc/letsencrypt/live/SERVER_NAME/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/SERVER_NAME/privkey.pem;
    include /opt/mailman/var/etc/nginx/options-ssl-nginx.conf;
    ssl_dhparam /opt/mailman/var/etc/nginx/dhparam.pem;

    if ($scheme != "https") {
      return 301 https://$host$request_uri;
    }

    location /static/ {
      alias /opt/mailman/mailman-suite/mailman-suite_project/static/;
    }

    location / {
      proxy_set_header        Host $host;
      proxy_set_header        X-Real-IP $remote_addr;
      proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header        X-Forwarded-Proto $scheme;

      proxy_pass http://localhost:8000;
    }
  }

  server {
      if ($host = SERVER_NAME) {
          return 301 https://$host$request_uri;
      }

      server_name SERVER_NAME;

      listen 80;
      return 404;
  }

SSL certificates / letsencrypt
##############################

Certbot will not be able to automatically configure the nginx proxy.

* Use ``certbot certonly`` to create new certificates
* The easiest certificate challenge is to let certbot stand up a
  temporary http server. The Zimbra proxy is blocking port 80, of
  course, so I stop the proxy temporarily and run certbot. (``zmproxyctl stop``)
  (Avoid this by using the DNS challenge if you prefer.)

Zimbra Postfix configuration
############################

The plan is to create *local configuration variables* in Zimbra as much as
possible, configured with the ``zmlocalconfig`` tool.

A small block of configuration is inserted in /opt/zimbra/conf/zmconfigd.cf
to insert the local configuration into main.cf.

Some Zimbra configuration files were created to support ``postconf`` items
that have a list of values.

.. note:: Since (most likely) Zimbra will be supporting domains that are
  also used by mailman, it's essential to use the ``virtual alias domain``
  configuration specified
  `in the mailman mta configuration here <https://docs.mailman3.org/projects/mailman/en/latest/src/mailman/docs/mta.html#unusual-postfix-configuration>`_.

Insert these lines in the ``SECTION mta`` part of zmconfigd.cf just before ``RESTART mta``:
::

  # Inserted for mailman3 integration
  # These "comment" lines in zmconfigd.cf cause "unknown" warnings, so 
  # best leave them out. (That's a shame.)
  POSTCONF recipient_delimiter LOCAL zimbraLocalMtaRecipientDelimiter
  POSTCONF owner_request_special LOCAL zimbraLocalMtaOwnerRequestSpecial
  POSTCONF transport_maps FILE zmconfigd/smtpd_transport_maps.cf
  POSTCONF local_recipient_maps FILE zmconfigd/smtpd_local_recipient_maps.cf
  POSTCONF relay_domains LOCAL zimbraLocalMtaRelayDomains
  POSTCONF virtual_alias_maps FILE zmconfigd/smtpd_virtual_alias_maps.cf
  # ---- end mailman 3 integration ----------------

Zimbra mta settings
###################

recipient_delimiter
===================

Set the local value::

  zmlocalconfig -e zimbraLocalMtaRecipientDelimiter="+"

owner_request_special
=====================

Set the local value::

  zmlocalconfig -e zimbraLocalMtaOwnerRequestSpecial=no

transport_maps
==============

Create config file zmconfigd/smtpd_transport_maps.cf
::

  %%zimbraMtaTransportMaps%%
  %%zimbraLocalMtaTransportMaps%%

Set the local value::

  zmlocalconfig -e zimbraLocalMtaTransportMaps="lmdb:/opt/mailman/var/data/postfix_lmtp"

local_recipient_maps
====================

Create config file zmconfd/smtpd_local_recipient_maps.cf
::

  proxy:unix:passwd.byname $alias_maps
  %%zmLocalMtaLocalRecipientMaps%%

Set the local value::

  zmlocalconfig -e zimbraLocalMtaLocalRecipientMaps="lmdb:/opt/mailman/var/data/postfix_lmtp"

relay_domains
=============

Set the local value::

  zmlocalconfig -e zimbraLocalMtaRelayDomains="lmdb:/opt/mailman/var/data/postfix_domains"

virtual_alias_maps
==================

.. note:: You may need to "touch" or create the file /opt/mailman/var/data/postfix_vmap
  and run postmap manually to create the .lmdb file the first time.
  (My initial mailman launch did not create the postfix_vmap virtual alias map.)

Using virtual domains is essential when Zimbra Postfix is likely to be managing mailboxes
in domains that are also used for mailman mailing lists.

Create config file zmconfigd/smtpd_virtual_alias_maps::

  %%zimbraMtaVirtualAliasMaps%%
  %%zimbraLocalMtaVirtualAliasMaps%%

Set the local value::

  zmlocalconfig -e zimbraLocalMtaVirtualAliasMaps="lmdb:/opt/mailman/var/data/postfix_vmap"

Mailman setup
#############

* Make a link to Zimbra's instance of Postfix's ``postmap`` program
  in /usr/sbin, or somewhere on mailman's path.
* ``postmap`` will create lmdb files rather than hash in the current
  version of postfix

Other mailman setup follows the standard installation guide. Zimbra's mta
should have an account login for django to generate outgoing mail.

